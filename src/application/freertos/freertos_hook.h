#ifndef _FREERTOS_HOOK_H_
#define _FREERTOS_HOOK_H_

#include "FreeRTOS.h"
#include "task.h"
#include <stdio.h>

void vApplicationMallocFailedHook(void);
void vApplicationStackOverflowHook(xTaskHandle pxTask, char *pcTaskName);
void vAssertCalled(const char *pcFileName, unsigned long ulLine);

#endif /*_FREERTOS_HOOK_H_*/
