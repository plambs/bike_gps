#include "FreeRTOS.h"
#include "task.h"
#include <stdio.h>
#include "freertos_hook.h"

void vApplicationMallocFailedHook(void) {
	printf("Malloc failed hook\n");
	taskDISABLE_INTERRUPTS();
	for(;;);
}

void vApplicationStackOverflowHook(xTaskHandle pxTask, char *pcTaskName) {
	(void) pcTaskName;
	(void) pxTask;
	printf("Stackoverflow in task: %s\n", pcTaskName);
	taskDISABLE_INTERRUPTS();
	for(;;);
}

void vAssertCalled(const char *pcFileName, unsigned long ulLine) {
	static portBASE_TYPE xPrinted = pdFALSE;
	volatile uint32_t ulSetToNonZeroInDebuggerToContinue = 0;

	/* Parameters are not used. */
	( void ) ulLine;
	( void ) pcFileName;

	taskENTER_CRITICAL();
	{
		/* You can step out of this function to debug the assertion by using
		   the debugger to set ulSetToNonZeroInDebuggerToContinue to a non-zero
		   value. */
		printf("Assert: %s - l%ld\n", pcFileName, ulLine);
		while( ulSetToNonZeroInDebuggerToContinue == 0 )
		{
		}
	}
	taskEXIT_CRITICAL();
}
