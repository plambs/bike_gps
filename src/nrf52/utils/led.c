#include <stdbool.h>
#include <stdint.h>
//#include "nrf.h"
#include "bsp.h"
#include "led.h"

int led_init(void)
{
	/* Init LED_1 and LED_2 GPIO*/
	nrf_gpio_cfg_output(LED_1_GPIO);
	nrf_gpio_cfg_output(LED_2_GPIO);

	/* Set led off */
	nrf_gpio_pin_write(LED_1_GPIO, LED_OFF);
	nrf_gpio_pin_write(LED_2_GPIO, LED_OFF);
	return 0;
}

int led_set_state(int led, bool state)
{
	nrf_gpio_pin_write(led, state);
	return 0;
}

int led_toggle(int led)
{
	nrf_gpio_pin_toggle(led);
	return 0;
}

