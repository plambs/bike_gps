/*
Copyright (C) 2020 LAMBS Pierre-Antoine

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _GUI_H_
#define _GUI_H

typedef enum
{
	E_PARAM_SPEED = 0,
	E_PARAM_HEART_RATE,
	E_PARAM_TEMPERATURE,
	E_PARAM_CADENCE,
	E_PARAM_ALTITUDE,
	E_PARAM_BATTERY,
	E_PARAM_POWER_LEFT,
	E_PARAM_POWER_RIGHT,
	E_PARAM_MAX, // Must be last
} E_parameter_id;

int gui_init(void);
int gui_update_param(E_parameter_id param_id, int value);

#endif
