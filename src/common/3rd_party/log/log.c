/*
 * This code was extracted and heavily modified from the libcommon 
 * that you can find at the address https://gitlab.com/plambs/libcommon
 * The lib is release under MIT license.
 */

#include <strings.h>
#include <stdarg.h>
#include <stdbool.h>
#include <time.h>
#include <stdio.h>
#include <inttypes.h>

#include "log.h"

#define MAX_LOG_LENGTH 256

//Log color
#define NORMAL    "\x1B[0m"
#define GREEN     "\x1B[32m"
#define BLACKBOLD "\033[1m"
#define REDBOLD   "\033[31m"

struct{
	bool is_initialized;
	bool use_color;
	log_get_time_handler get_time_handler;
} g_log = {
	.is_initialized = false,
	.use_color = false,
};

void _write_log(const char * filename, const char * function, int line, const char * format, ...){
	va_list args;
	uint32_t time_ms = g_log.get_time_handler();
	char log_va_list_buffer[MAX_LOG_LENGTH];

	if(!g_log.is_initialized){
		return;
	}

	/*
	 * Get va_args into log_va_list_buffer
	 */
	va_start( args,format);
	vsnprintf( log_va_list_buffer, sizeof(log_va_list_buffer), format, args);
	va_end( args);

	/*
	 * Print log on console
	 */
	if( g_log.use_color == true ){
		printf( "[%"PRIu32".%03"PRIu32"d]["BLACKBOLD"%s:%d:%s"NORMAL"] "REDBOLD"%s"NORMAL, time_ms/1000, time_ms%1000, filename, line, function, log_va_list_buffer);
	}else{
		printf( "[%"PRIu32".%03"PRIu32"][%s:%d:%s] %s", time_ms/1000, time_ms%1000, filename, line, function, log_va_list_buffer);
	}

	return;
}

int log_init(log_get_time_handler time_handler){
	if(time_handler == NULL)
	{
		return -1;
	}

	g_log.get_time_handler = time_handler;
	g_log.is_initialized = true;

	return 0;
}

int log_set_color_log(bool value){
	if(g_log.is_initialized == false){
		return -1;
	}

	g_log.use_color = value;

	write_log("Set log color: %s", value ? "ON" : "OFF");
	return 0;
}
