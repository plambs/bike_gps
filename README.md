# Bike GPS

## Instructions
### Clone the repository
```
git clone --recurse-submodules https://gitlab.com/plambs/bike_gps.git
```

### Build and flash the application
```
make -C src/common/3rd_party/libopencm3
make -C src/application
make flash -C src/application
```

### Build and flash the nrf52 firmware
```
cd src/nrf52
sh setup_sdk.sh
make
make flash
```

## Architecture
* **src/application**: application firmware for the stm32
* **src/nrf52**: firmware of the nrf52382 to get the speed, hr and cadence from ANT+ sensors
* **src/common**: all code shared between project (uart logging, freertos, config...).
* **src/common/3rd_party**: all 3rd party source code (as submodule).
* **documentation**: some datasheet and pinout/picture of the board.

## License
All 3rd party code with their own license are in src/common/3rd_party. \
All other code is released under the GPLv3, see LICENSE.md.
