/*
Copyright (C) 2020 LAMBS Pierre-Antoine

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _ILI9341_DRIVER_H_
#define _ILI9341_DRIVER_H_

#define ILI9341_RGB565(red, green, blue) (((red & 0x1F) << 11) | ((green & 0x3F) << 5) | (blue & 0x1F))

#define ILI9341_COM_SPI 1
#define ILI9341_COM_PARALLELE 2

/* Choose the default screen communcation if not set */
#ifndef ILI9341_COMMUNICATION_TYPE
	//#define ILI9341_COMMUNICATION_TYPE ILI9341_COM_SPI
	#define ILI9341_COMMUNICATION_TYPE ILI9341_COM_PARALLELE
#endif

typedef enum {
	E_TOUCH_ACTION_PRESS = 0,
	E_TOUCH_ACTION_RELEASE,
} E_ili9341_touch_action;

/*TODO pass screen size to the init and maybe orientation ?*/
int ili9341_init(void);
int ili9341_fill_screen(uint16_t color);
int ili9341_set_pixel(uint16_t column, uint16_t row, uint16_t color);

int ili9341_set_pixel_frame(uint16_t column_start, uint16_t column_end, uint16_t row_start, uint16_t row_end);
int ili9341_send_pixel_data(uint16_t *data, uint32_t nb_data);
int ili9341_send_pixel_data_continue(uint16_t *data, uint32_t nb_data);
int ili9341_set_pixel_frame_test(uint16_t x_start, uint16_t x_end, uint16_t y_start, uint16_t y_end, uint16_t *data, uint32_t nb_data);

typedef void (*touch_irq_handler)(E_ili9341_touch_action action);

int ili9341_register_touch_irq_handler(touch_irq_handler irq_handler);
int ili9341_get_touch_coordinates(uint16_t *valueX, uint16_t *valueY);

typedef enum {
	E_SCREEN_ROTATION_POS_0 = 0,
	E_SCREEN_ROTATION_POS_1,
	E_SCREEN_ROTATION_POS_2,
	E_SCREEN_ROTATION_POS_3,
} E_ili9341_screen_rotation_pos;
int ili9341_rotate_screen(E_ili9341_screen_rotation_pos pos);

/*Test function to verify if the screen work*/
int ili9341_print_test(void);

#endif /*_ILI9341_DRIVER_H_*/
