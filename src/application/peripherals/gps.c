/*
Copyright (C) 2020 LAMBS Pierre-Antoine

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <string.h>

#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/usart.h>
#include <libopencm3/cm3/nvic.h>

#include "FreeRTOS.h"
#include "task.h"
#include "portmacro.h"
#include "queue.h"
#include "minmea.h"
#include "log.h"

#include "gps.h"

static QueueHandle_t recv_char_queue = NULL;

static int _parse_nmea(char *line)
{
	enum minmea_sentence_id id;
	id = minmea_sentence_id(line, false);
	switch (id) {
		case MINMEA_SENTENCE_RMC:
			{
				struct minmea_sentence_rmc frame;
				if (minmea_parse_rmc(&frame, line)) {
					write_log("$RMC: raw coordinates and speed: (%ld/%ld,%ld/%ld) %ld/%ld\n",
							frame.latitude.value, frame.latitude.scale,
							frame.longitude.value, frame.longitude.scale,
							frame.speed.value, frame.speed.scale);
					write_log("$RMC fixed-point coordinates and speed scaled to three decimal places: (%ld,%ld) %ld\n",
							minmea_rescale(&frame.latitude, 1000),
							minmea_rescale(&frame.longitude, 1000),
							minmea_rescale(&frame.speed, 1000));
					write_log("$RMC floating point degree coordinates and speed: (%f,%f) %f\n",
							minmea_tocoord(&frame.latitude),
							minmea_tocoord(&frame.longitude),
							minmea_tofloat(&frame.speed));
				}
			} break;

		case MINMEA_SENTENCE_GGA:
			{
				struct minmea_sentence_gga frame;
				if (minmea_parse_gga(&frame, line)) {
					write_log("$GGA: fix quality: %d\n", frame.fix_quality);
				}
			} break;

		case MINMEA_SENTENCE_GSV:
			{
				struct minmea_sentence_gsv frame;
				if (minmea_parse_gsv(&frame, line)) {
					write_log("$GSV: message %d of %d\n", frame.msg_nr, frame.total_msgs);
					write_log("$GSV: sattelites in view: %d\n", frame.total_sats);
					for (int i = 0; i < 4; i++)
						write_log("$GSV: sat nr %d, elevation: %d, azimuth: %d, snr: %d dbm\n",
								frame.sats[i].nr,
								frame.sats[i].elevation,
								frame.sats[i].azimuth,
								frame.sats[i].snr);
				}
			} break;
    	case MINMEA_SENTENCE_GSA:
    	case MINMEA_SENTENCE_GLL:
    	case MINMEA_SENTENCE_GST:
    	case MINMEA_SENTENCE_VTG:
    	case MINMEA_SENTENCE_ZDA:
			break;
		default:
			write_log("Sentence invalid, id: %d, line: %s\n", id, line);
			break;
	}

	return 0;
}

static void _nmea_decode_task(void *param)
{
	uint8_t cpt = 0;
	char recv_byte;
	char line[MINMEA_MAX_LENGTH];

	(void)param; /* avoid unused parameter warning */

	while(1)
	{
		if(xQueueReceive(recv_char_queue, &recv_byte, portMAX_DELAY) == pdPASS)
		{
			if(cpt >= MINMEA_MAX_LENGTH)
			{
				write_log("Avoid buffer overflow\n");
				memset(line, '\0', sizeof(line));
				cpt = 0;
				continue;
			}

			line[cpt] = recv_byte;

			/* Parse the line if we are at the end */
			if(recv_byte == '\n' && line[cpt -1] == '\r')
			{
				//write_log("%s\n", line);
				_parse_nmea(line);
				memset(line, '\0', sizeof(line));
				cpt = 0;
			}
			else
			{
				cpt++;
			}
		}
	}
}

/* Uart ISR handler */
void usart2_isr(void)
{
	char data;
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;

	/* Check if we were called because of RXNE. */
	if (((USART_CR1(USART2) & USART_CR1_RXNEIE) != 0) &&
	    ((USART_SR(USART2) & USART_SR_RXNE) != 0)) {

		/* Retrieve the data from the peripheral. */
		data = usart_recv(USART2);

		if(recv_char_queue != NULL)
		{
			xQueueSendFromISR(recv_char_queue, &data, &xHigherPriorityTaskWoken );
		}
	}

	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

int gps_init(void)
{
	/*Use serial 2 - PA2 (TX) and PA3 (RX) gpio*/
	write_log("Init GPS usart\n");

	rcc_periph_clock_enable(RCC_GPIOA);
	rcc_periph_clock_enable(RCC_USART2);

	/* Enable the USART2 interrupt. */
	nvic_enable_irq(NVIC_USART2_IRQ);

	/* Setup GPIO pin TX and RX (AF7) */
	gpio_mode_setup(GPIOA, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO2 | GPIO3);
	gpio_set_af(GPIOA, GPIO_AF7, GPIO2 | GPIO3);

	/* Setup UART parameters. */
	usart_set_baudrate(USART2, 9600);
	usart_set_databits(USART2, 8);
	usart_set_stopbits(USART2, USART_STOPBITS_1);
	usart_set_parity(USART2, USART_PARITY_NONE);
	usart_set_flow_control(USART2, USART_FLOWCONTROL_NONE);
	usart_set_mode(USART2, USART_MODE_RX);

	/* Set USART 2 interrupt priority (255 = lowest) */
	nvic_set_priority(NVIC_USART2_IRQ, 255);

	/* Enable the USART2 interrupt. */
	nvic_enable_irq(NVIC_USART2_IRQ);

	/* Enable USART2 Receive interrupt. */
	usart_enable_rx_interrupt(USART2);

	/* Finally enable the USART. */
	usart_enable(USART2);

	recv_char_queue = xQueueCreate(32, sizeof(char));
	if(recv_char_queue == NULL)
	{
		write_log("Create gps recv queue fail\n");
		return -1;
	}

	if(xTaskCreate(_nmea_decode_task, "nmea", 1024, NULL, 5, NULL) != pdPASS)
	{
		write_log("Start nmea decode task FAILED\n");
		return -2;
	}

	return 0;
}
