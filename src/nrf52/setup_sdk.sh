#!/bin/sh

DEST_SDK_FOLDER="sdk"
SDK_FOLDER_NAME="nRF5_SDK_17.0.2_d674dde"
SDK_ARCHIVE_NAME="nRF5SDK1702d674dde.zip"
SDK_ADDRESS="https://www.nordicsemi.com/-/media/Software-and-other-downloads/SDKs/nRF5/Binaries/${SDK_ARCHIVE_NAME}"

if [ -e ${DEST_SDK_FOLDER} ]
then
	echo "${DEST_SDK_FOLDER} already exist, abort"
	exit 1
fi

echo "Setup nrf52 SDK"
# Download, unzip and install in the correct folder
wget --progress=bar ${SDK_ADDRESS} 
unzip ${SDK_ARCHIVE_NAME}
mv ${SDK_FOLDER_NAME} ${DEST_SDK_FOLDER}

# Cleanup
rm -rf ${SDK_FOLDER_NAME}
rm -f ${SDK_ARCHIVE_NAME}

echo "Done"

exit 0
