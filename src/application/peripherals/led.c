#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include "log.h"

#include "led.h"

#define RCC_LED_D2  RCC_GPIOA
#define PORT_LED_D2 GPIOA
#define PIN_LED_D2  GPIO6

#define RCC_LED_D3  RCC_GPIOA
#define PORT_LED_D3 GPIOA
#define PIN_LED_D3  GPIO7

int led_set_state(E_led_id led_id, bool state)
{
	uint32_t port = 0, pin = 0;

	switch(led_id)
	{
		case E_LED_D2:
			port = PORT_LED_D2;
			pin = PIN_LED_D2;
			break;
		case E_LED_D3:
			port = PORT_LED_D3;
			pin = PIN_LED_D3;
			break;
		default:
			common_die(-1, "led_id (%d) is invalid\n", led_id);
			break;
	}

	if(state)
	{
		gpio_set(port, pin);
	}
	else
	{
		gpio_clear(port, pin);
	}

	return 0;
}

int led_toogle(E_led_id led_id)
{
	uint32_t port = 0, pin = 0;

	switch(led_id)
	{
		case E_LED_D2:
			port = PORT_LED_D2;
			pin = PIN_LED_D2;
			break;
		case E_LED_D3:
			port = PORT_LED_D3;
			pin = PIN_LED_D3;
			break;
		default:
			common_die(-1, "led_id (%d) is invalid\n", led_id);
			break;
	}

	gpio_toggle(port, pin);

	return 0;
}

int led_init(void)
{
	/* Setup LED D2 gpio */
	rcc_periph_clock_enable(RCC_LED_D2);
	gpio_mode_setup(PORT_LED_D2, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, PIN_LED_D2);

	/* Setup LED D3 gpio */
	rcc_periph_clock_enable(RCC_LED_D3);
	gpio_mode_setup(PORT_LED_D3, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, PIN_LED_D3);

	/* Turn led off */
	gpio_set(PORT_LED_D2, PIN_LED_D2);
	gpio_set(PORT_LED_D3, PIN_LED_D3);

	return 0;
}
