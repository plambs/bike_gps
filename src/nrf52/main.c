#include <stdbool.h>
#include <stdint.h>
#include "nrf_delay.h"
#include "led.h"
#include "log_uart.h"
#include "log.h"

int main(void)
{
	log_uart_init();
	led_init();

	write_log("Application is starting\r\n");

	/* Toggle LEDs. */
	while (true)
	{
		led_toggle(LED_1_GPIO);
		write_log("Hello World\r\n");
		nrf_delay_ms(500);
	}
}
