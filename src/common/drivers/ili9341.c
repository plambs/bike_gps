/*
Copyright (C) 2020 LAMBS Pierre-Antoine

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/usart.h>
#include <libopencm3/stm32/spi.h>

#include <stdio.h>
#include <string.h>

#include "FreeRTOS.h"
#include "task.h"

#include "ili9341.h"

#if (ILI9341_COMMUNICATION_TYPE ==  ILI9341_COM_SPI)

/* Backlight (PB_1) */
#define ILI9341_BACKLIGHT_RCC RCC_GPIOB
#define ILI9341_BACKLIGHT_PORT GPIOB
#define ILI9341_BACKLIGHT_PIN GPIO1

/* Chip select gpio (PB_7) */
#define ILI9341_SPI_CS_RCC RCC_GPIOD
#define ILI9341_SPI_CS_PORT GPIOD
#define ILI9341_SPI_CS_PIN GPIO7

/* Reset gpio (PD_13) */
#define ILI9341_RST_RCC RCC_GPIOD
#define ILI9341_RST_PORT GPIOD
#define ILI9341_RST_PIN GPIO13

/* Data/Command (dc) gpio (PD_5) */
#define ILI9341_DC_RCC RCC_GPIOD
#define ILI9341_DC_PORT GPIOD
#define ILI9341_DC_PIN GPIO5

#elif (ILI9341_COMMUNICATION_TYPE == ILI9341_COM_PARALLELE)

/*RS pin is DC (data/command mode)*/
/*RD is read screen in parallele mode*/
/*WR is write screen in parallele mode*/

/* Backlight (PB_1) */
#define ILI9341_BACKLIGHT_RCC RCC_GPIOB
#define ILI9341_BACKLIGHT_PORT GPIOB
#define ILI9341_BACKLIGHT_PIN GPIO1

/* Touch interrupt (PC_5) */
#define ILI9341_TOUCH_INT_RCC RCC_GPIOC
#define ILI9341_TOUCH_INT_PORT GPIOC
#define ILI9341_TOUCH_INT_PIN GPIO5

/* SPI MISO (PB_14) */
#define ILI9341_MISO_RCC RCC_GPIOB
#define ILI9341_MISO_PORT GPIOB
#define ILI9341_MISO_PIN GPIO14

/* SPI MOSI (PB_15) */
#define ILI9341_MOSI_RCC RCC_GPIOB
#define ILI9341_MOSI_PORT GPIOB
#define ILI9341_MOSI_PIN GPIO15

/* SCS (touch chip select) (PB_12) */
#define ILI9341_TOUCH_CS_RCC RCC_GPIOB
#define ILI9341_TOUCH_CS_PORT GPIOB
#define ILI9341_TOUCH_CS_PIN GPIO12

/* SPI SCK (clock) (PB_13) */
#define ILI9341_SCK_RCC RCC_GPIOB
#define ILI9341_SCK_PORT GPIOB
#define ILI9341_SCK_PIN GPIO13

/* Display RS (data/command) mode (PD_13) */
#define ILI9341_DC_RCC RCC_GPIOD
#define ILI9341_DC_PORT GPIOD
#define ILI9341_DC_PIN GPIO13

/* CS_M (screen display chip select) (PD_7) */
#define ILI9341_SCREEN_CS_RCC RCC_GPIOD
#define ILI9341_SCREEN_CS_PORT GPIOD
#define ILI9341_SCREEN_CS_PIN GPIO7

/* WR (screen write for parallele mode) (PD_5) */
#define ILI9341_SCREEN_WRITE_RCC RCC_GPIOD
#define ILI9341_SCREEN_WRITE_PORT GPIOD
#define ILI9341_SCREEN_WRITE_PIN GPIO5

/* RD (read screen for parallele mode) (PD_4) */
#define ILI9341_SCREEN_READ_RCC RCC_GPIOD
#define ILI9341_SCREEN_READ_PORT GPIOD
#define ILI9341_SCREEN_READ_PIN GPIO4

/* Data line */

/* DB00 (PD_14) */
#define ILI9341_DB00_RCC RCC_GPIOD
#define ILI9341_DB00_PORT GPIOD
#define ILI9341_DB00_PIN GPIO14

/* DB01 (PD_15) */
#define ILI9341_DB01_RCC RCC_GPIOD
#define ILI9341_DB01_PORT GPIOD
#define ILI9341_DB01_PIN GPIO15

/* DB02 (PD_0) */
#define ILI9341_DB02_RCC RCC_GPIOD
#define ILI9341_DB02_PORT GPIOD
#define ILI9341_DB02_PIN GPIO0

/* DB03 (PD_1) */
#define ILI9341_DB03_RCC RCC_GPIOD
#define ILI9341_DB03_PORT GPIOD
#define ILI9341_DB03_PIN GPIO1

/* DB04 (PE_7) */
#define ILI9341_DB04_RCC RCC_GPIOE
#define ILI9341_DB04_PORT GPIOE
#define ILI9341_DB04_PIN GPIO7

/* DB05 (PE_8) */
#define ILI9341_DB05_RCC RCC_GPIOE
#define ILI9341_DB05_PORT GPIOE
#define ILI9341_DB05_PIN GPIO8

/* DB06 (PE_9) */
#define ILI9341_DB06_RCC RCC_GPIOE
#define ILI9341_DB06_PORT GPIOE
#define ILI9341_DB06_PIN GPIO9

/* DB07 (PE_10) */
#define ILI9341_DB07_RCC RCC_GPIOE
#define ILI9341_DB07_PORT GPIOE
#define ILI9341_DB07_PIN GPIO10

/* DB08 (PE_11) */
#define ILI9341_DB08_RCC RCC_GPIOE
#define ILI9341_DB08_PORT GPIOE
#define ILI9341_DB08_PIN GPIO11

/* DB09 (PE_12) */
#define ILI9341_DB09_RCC RCC_GPIOE
#define ILI9341_DB09_PORT GPIOE
#define ILI9341_DB09_PIN GPIO12

/* DB10 (PE_13) */
#define ILI9341_DB10_RCC RCC_GPIOE
#define ILI9341_DB10_PORT GPIOE
#define ILI9341_DB10_PIN GPIO13

/* DB11 (PE_14) */
#define ILI9341_DB11_RCC RCC_GPIOE
#define ILI9341_DB11_PORT GPIOE
#define ILI9341_DB11_PIN GPIO14

/* DB12 (PE_15) */
#define ILI9341_DB12_RCC RCC_GPIOE
#define ILI9341_DB12_PORT GPIOE
#define ILI9341_DB12_PIN GPIO15

/* DB13 (PD_8) */
#define ILI9341_DB13_RCC RCC_GPIOD
#define ILI9341_DB13_PORT GPIOD
#define ILI9341_DB13_PIN GPIO8

/* DB14 (PD_9) */
#define ILI9341_DB14_RCC RCC_GPIOD
#define ILI9341_DB14_PORT GPIOD
#define ILI9341_DB14_PIN GPIO9

/* DB15 (PD_10) */
#define ILI9341_DB15_RCC RCC_GPIOD
#define ILI9341_DB15_PORT GPIOD
#define ILI9341_DB15_PIN GPIO10

/*RST (reset) is connected directly on the stm32 NRST line*/

#endif /*ILI9341_COMMUNICATION_TYPE == ILI9341_COM_PARALLELE*/

#define ILI9341_SCREEN_COLUMN 240
#define ILI9341_SCREEN_ROW 320

static bool is_initialized = false;
static touch_irq_handler registred_user_touch_irq_handler = NULL;
static E_ili9341_screen_rotation_pos rotation_pos = E_SCREEN_ROTATION_POS_0;

typedef enum {
	E_ILI9341_MODE_COMMAND = 0,
	E_ILI9341_MODE_DATA = 1,
} E_ili9341_display_mode;

static int _change_display_mode(E_ili9341_display_mode new_mode)
{
	switch(new_mode)
	{
		case E_ILI9341_MODE_COMMAND:
			gpio_clear(ILI9341_DC_PORT, ILI9341_DC_PIN);
			break;
		case E_ILI9341_MODE_DATA:
			gpio_set(ILI9341_DC_PORT, ILI9341_DC_PIN);
			break;
		default:
			/*error*/
			return -1;
			break;
	}

	return 0;
}

#if (ILI9341_COMMUNICATION_TYPE == ILI9341_COM_SPI)
static void _reset_display(void)
{
	/* Reset the display, reset is active low */
	gpio_clear(ILI9341_RST_PORT, ILI9341_RST_PIN);
	vTaskDelay(100);
	gpio_set(ILI9341_RST_PORT, ILI9341_RST_PIN);
	vTaskDelay(100);

	return;
}
#endif

#if (ILI9341_COMMUNICATION_TYPE == ILI9341_COM_SPI)
static void _init_spi(void)
{
	/* Enable the GPIO ports whose pins we are using */
	rcc_periph_clock_enable(RCC_GPIOB);
	rcc_periph_clock_enable(RCC_SPI2);

	spi_reset(SPI2);

	/* Configure GPIO, CS=PB_12, MOSI=PB_15, SCK=PB_13, MISO=PB14*/

	/* Output GPIO */
	gpio_mode_setup(GPIOB, GPIO_MODE_AF, GPIO_PUPD_PULLDOWN, GPIO13 | GPIO14 | GPIO15);

	/* Set alternative function (spi) for GPIO */
	gpio_set_af(GPIOB, GPIO_AF5, /*GPIO12 |*/ GPIO13 | GPIO14 | GPIO15);

	/* SCK + MOSI -> 25MHZ */
	gpio_set_output_options(GPIOB, GPIO_OTYPE_PP, GPIO_OSPEED_25MHZ, GPIO13 | GPIO15);

	/* Explicitly disable I2S in favour of SPI operation */
	SPI2_I2SCFGR = 0;

	spi_init_master(SPI2, SPI_CR1_BAUDRATE_FPCLK_DIV_2, SPI_CR1_CPOL_CLK_TO_0_WHEN_IDLE, SPI_CR1_CPHA_CLK_TRANSITION_1, SPI_CR1_DFF_8BIT, SPI_CR1_MSBFIRST);

	spi_enable(SPI2);

	return;
}
#endif

#if(ILI9341_COMMUNICATION_TYPE == ILI9341_COM_SPI)
static int _send_cmd_spi(uint8_t cmd)
{
	/* Select CS */
	gpio_clear(GPIOB, GPIO12);

	/* Send command */
	_change_display_mode(E_ILI9341_MODE_COMMAND);
	spi_send(SPI2, cmd);
	(void)spi_read(SPI2);

	/* Clear CS */
	gpio_set(GPIOB, GPIO12);

	return 0;
}

static int _send_data8_spi(uint8_t data)
{
	/* Select CS */
	gpio_clear(GPIOB, GPIO12);

	/* Send data */
	_change_display_mode(E_ILI9341_MODE_DATA);
	spi_send(SPI2, data);
	(void)spi_read(SPI2);

	/* Clear CS */
	gpio_set(GPIOB, GPIO12);

	return 0;
}

static int _send_data16_spi(uint16_t data)
{
	_send_data8_spi(data >> 8);
	_send_data8_spi(data & 0xFF);
	return 0;
}
#endif

#if(ILI9341_COMMUNICATION_TYPE == ILI9341_COM_PARALLELE)
#define ILI9341_SET_PARALLELE_BIT(value, offset, port, pin) (value & (1 << offset) ? gpio_set(port, pin) : gpio_clear(port, pin))
static int _set_parallele_port_value(uint16_t value)
{
	ILI9341_SET_PARALLELE_BIT(value, 0, ILI9341_DB00_PORT, ILI9341_DB00_PIN);
	ILI9341_SET_PARALLELE_BIT(value, 1, ILI9341_DB01_PORT, ILI9341_DB01_PIN);
	ILI9341_SET_PARALLELE_BIT(value, 2, ILI9341_DB02_PORT, ILI9341_DB02_PIN);
	ILI9341_SET_PARALLELE_BIT(value, 3, ILI9341_DB03_PORT, ILI9341_DB03_PIN);
	ILI9341_SET_PARALLELE_BIT(value, 4, ILI9341_DB04_PORT, ILI9341_DB04_PIN);
	ILI9341_SET_PARALLELE_BIT(value, 5, ILI9341_DB05_PORT, ILI9341_DB05_PIN);
	ILI9341_SET_PARALLELE_BIT(value, 6, ILI9341_DB06_PORT, ILI9341_DB06_PIN);
	ILI9341_SET_PARALLELE_BIT(value, 7, ILI9341_DB07_PORT, ILI9341_DB07_PIN);
	ILI9341_SET_PARALLELE_BIT(value, 8, ILI9341_DB08_PORT, ILI9341_DB08_PIN);
	ILI9341_SET_PARALLELE_BIT(value, 9, ILI9341_DB09_PORT, ILI9341_DB09_PIN);
	ILI9341_SET_PARALLELE_BIT(value, 10, ILI9341_DB10_PORT, ILI9341_DB10_PIN);
	ILI9341_SET_PARALLELE_BIT(value, 11, ILI9341_DB11_PORT, ILI9341_DB11_PIN);
	ILI9341_SET_PARALLELE_BIT(value, 12, ILI9341_DB12_PORT, ILI9341_DB12_PIN);
	ILI9341_SET_PARALLELE_BIT(value, 13, ILI9341_DB13_PORT, ILI9341_DB13_PIN);
	ILI9341_SET_PARALLELE_BIT(value, 14, ILI9341_DB14_PORT, ILI9341_DB14_PIN);
	ILI9341_SET_PARALLELE_BIT(value, 15, ILI9341_DB15_PORT, ILI9341_DB15_PIN);
	return 0;
}

static int _send_cmd_parallele(uint8_t cmd)
{
	/*Chip select*/
	gpio_clear(ILI9341_SCREEN_CS_PORT, ILI9341_SCREEN_CS_PIN);

	/*Reset WR pin*/
	gpio_clear(ILI9341_SCREEN_WRITE_PORT, ILI9341_SCREEN_WRITE_PIN);

	_change_display_mode(E_ILI9341_MODE_COMMAND);

	_set_parallele_port_value((uint16_t) cmd);

	/*Set WR pin*/
	gpio_set(ILI9341_SCREEN_WRITE_PORT, ILI9341_SCREEN_WRITE_PIN);

	_set_parallele_port_value(0);

	/*Clean chip select*/
	gpio_set(ILI9341_SCREEN_CS_PORT, ILI9341_SCREEN_CS_PIN);

	return 0;
}

static int _send_data16_parallele(uint16_t data)
{
	/*Chip select*/
	gpio_clear(ILI9341_SCREEN_CS_PORT, ILI9341_SCREEN_CS_PIN);

	/*Reset WR pin*/
	gpio_clear(ILI9341_SCREEN_WRITE_PORT, ILI9341_SCREEN_WRITE_PIN);

	_change_display_mode(E_ILI9341_MODE_DATA);

	_set_parallele_port_value(data);

	/*Set WR pin*/
	gpio_set(ILI9341_SCREEN_WRITE_PORT, ILI9341_SCREEN_WRITE_PIN);

	_set_parallele_port_value(0);

	/*Clean chip select*/
	gpio_set(ILI9341_SCREEN_CS_PORT, ILI9341_SCREEN_CS_PIN);

	return 0;
}

static int _send_data8_parallele(uint8_t data)
{
	return _send_data16_parallele((uint16_t)data);
}

#endif

static int _send_cmd(uint8_t cmd)
{
#if (ILI9341_COMMUNICATION_TYPE == ILI9341_COM_SPI)
	return _send_cmd_spi(cmd);
#elif(ILI9341_COMMUNICATION_TYPE == ILI9341_COM_PARALLELE)
	return _send_cmd_parallele(cmd);
#endif
}


static int _send_data8(uint8_t data)
{
#if (ILI9341_COMMUNICATION_TYPE == ILI9341_COM_SPI)
	return _send_data8_spi(data);
#elif(ILI9341_COMMUNICATION_TYPE == ILI9341_COM_PARALLELE)
	return _send_data8_parallele(data);
#endif
}

static int _send_data16(uint16_t data)
{
#if (ILI9341_COMMUNICATION_TYPE == ILI9341_COM_SPI)
	return _send_data16_spi(data);
#elif(ILI9341_COMMUNICATION_TYPE == ILI9341_COM_PARALLELE)
	return _send_data16_parallele(data);
#endif
}

static int _change_display_rotation_pos(E_ili9341_screen_rotation_pos new_pos)
{
	switch(new_pos)
	{
		case E_SCREEN_ROTATION_POS_0:
			_send_cmd(0x36);
			_send_data8(0x40 | 0x08);
			break;
		case E_SCREEN_ROTATION_POS_1:
			_send_cmd(0x36);
			_send_data8(0x20 | 0x08);
			break;
		case E_SCREEN_ROTATION_POS_2:
			_send_cmd(0x36);
			_send_data8(0x80 | 0x08);
			break;
		case E_SCREEN_ROTATION_POS_3:
			_send_cmd(0x36);
			_send_data8(0x40 | 0x80 | 0x20 | 0x08);
			break;
		default:
			printf("Error new display rotation position is invalide (%d)\n", new_pos);
			return -1;
			break;
	}

	/* Save position for the touchscreen */
	rotation_pos = new_pos;
	return 0;
}

static void _send_ili9341_init_sequence(void)
{
	_send_cmd(0x01);

	_send_cmd(0xEF);

	_send_data8(0x03);
	_send_data8(0x80);
	_send_data8(0x02);

	_send_cmd(0xCF);
	_send_data8(0x00);
	_send_data8(0xC1);
	_send_data8(0x30);

	_send_cmd(0xED);
	_send_data8(0x64);
	_send_data8(0x03);
	_send_data8(0x12);
	_send_data8(0x81);

	_send_cmd(0xE8);
	_send_data8(0x85);
	_send_data8(0x00);
	_send_data8(0x78);

	_send_cmd(0xCB);
	_send_data8(0x39);
	_send_data8(0x2C);
	_send_data8(0x00);
	_send_data8(0x34);
	_send_data8(0x02);

	_send_cmd(0xF7);
	_send_data8(0x20);

	_send_cmd(0xEA);
	_send_data8(0x00);
	_send_data8(0x00);

	// PWCTR1
	_send_cmd(0xC0);
	_send_data8(0x23);

	// PWCTR2
	_send_cmd(0xC1);
	_send_data8(0x10);

	// VMCTR1
	_send_cmd(0xC5);
	_send_data8(0x3E);
	_send_data8(0x28);

	// VMCTR2
	_send_cmd(0xC7);
	_send_data8(0x86);

	// MADCTL (screen rotation)
	_change_display_rotation_pos(rotation_pos);

	// VSCRSADD
	_send_cmd(0x37);
	_send_data8(0x00);

	// Interface control
	_send_cmd(0xF6);
	_send_data8(0x00);
	_send_data8(0x00);
	_send_data8(0x00);

	// PIXFMT (Pixel format)
	_send_cmd(0x3A);
	_send_data8(0x55);
	//_send_data8(0x56);

	// FRMCTR1
	_send_cmd(0xB1);
	_send_data8(0x00);
	//_send_data8(0x18);
	_send_data8(0x10);

	// DFUNCTR
	_send_cmd(0xB6);
	_send_data8(0x08);
	_send_data8(0x82);
	_send_data8(0x27);
	_send_cmd(0xF2);
	_send_data8(0x00);

	// GAMMASET
	_send_cmd(0x26);
	_send_data8(0x01);

	// (Actual gamma settings)
	_send_cmd(0xE0);
	_send_data8(0x0F);
	_send_data8(0x31);
	_send_data8(0x2B);
	_send_data8(0x0C);
	_send_data8(0x0E);
	_send_data8(0x08);
	_send_data8(0x4E);
	_send_data8(0xF1);
	_send_data8(0x37);
	_send_data8(0x07);
	_send_data8(0x10);
	_send_data8(0x03);
	_send_data8(0x0E);
	_send_data8(0x09);
	_send_data8(0x00);
	_send_cmd(0xE1);
	_send_data8(0x00);
	_send_data8(0x0E);
	_send_data8(0x14);
	_send_data8(0x03);
	_send_data8(0x11);
	_send_data8(0x07);
	_send_data8(0x31);
	_send_data8(0xC1);
	_send_data8(0x48);
	_send_data8(0x08);
	_send_data8(0x0F);
	_send_data8(0x0C);
	_send_data8(0x31);
	_send_data8(0x36);
	_send_data8(0x0F);

	// Exit sleep mode.
	_send_cmd(0x11);

	vTaskDelay(200);

	// Display on.
	_send_cmd(0x29);
	vTaskDelay(200);

	// 'Normal' display mode.
	_send_cmd(0x13);

	return;
}

#include <libopencm3/cm3/nvic.h>
#include <libopencm3/stm32/exti.h>
static void _init_touch(void)
{
///* SPI MISO (PB_14) */
//#define ILI9341_MISO_RCC RCC_GPIO
//#define ILI9341_MISO_PORT GPIO
//#define ILI9341_MISO_PIN GPIO
//
///* SPI MOSI (PB_15) */
//#define ILI9341_MOSI_RCC RCC_GPIO
//#define ILI9341_MOSI_PORT GPIO
//#define ILI9341_MOSI_PIN GPIO
//
///* SCS (touch chip select) (PB_12) */
//#define ILI9341_TOUCH_CS_RCC RCC_GPIO
//#define ILI9341_TOUCH_CS_PORT GPIO
//#define ILI9341_TOUCH_CS_PIN GPIO
//
///* SPI SCK (clock) (PB_13) */
//#define ILI9341_SCK_RCC RCC_GPIO
//#define ILI9341_SCK_PORT GPIO
//#define ILI9341_SCK_PIN GPIO

	/* Init SPI chip select TODO need to be managed if using the same spi as screen */
	rcc_periph_clock_enable(ILI9341_TOUCH_CS_RCC);
	gpio_mode_setup(ILI9341_TOUCH_CS_PORT, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, ILI9341_TOUCH_CS_PIN);
	gpio_set(ILI9341_TOUCH_CS_PORT, ILI9341_TOUCH_CS_PIN);

	/* Enable the GPIO ports whose pins we are using */
	rcc_periph_clock_enable(RCC_GPIOB);
	rcc_periph_clock_enable(RCC_SPI2);

	spi_reset(SPI2);

	/* Configure GPIO, CS=PB_12, MOSI=PB_15, SCK=PB_13, MISO=PB14*/

	/* Output GPIO */
	gpio_mode_setup(GPIOB, GPIO_MODE_AF, GPIO_PUPD_PULLDOWN, GPIO13 | GPIO14 | GPIO15);

	/* Set alternative function (spi) for GPIO */
	gpio_set_af(GPIOB, GPIO_AF5, /*GPIO12 |*/ GPIO13 | GPIO14 | GPIO15);

	/* SCK + MOSI -> 25MHZ */
	gpio_set_output_options(GPIOB, GPIO_OTYPE_PP, GPIO_OSPEED_25MHZ, GPIO13 | GPIO15);

	/* Explicitly disable I2S in favour of SPI operation */
	SPI2_I2SCFGR = 0;

	spi_init_master(SPI2, SPI_CR1_BAUDRATE_FPCLK_DIV_32, SPI_CR1_CPOL_CLK_TO_0_WHEN_IDLE, SPI_CR1_CPHA_CLK_TRANSITION_1, SPI_CR1_DFF_16BIT, SPI_CR1_MSBFIRST);

	spi_enable(SPI2);

	/*Select CS*/
	//gpio_clear(ILI9341_TOUCH_CS_PORT, ILI9341_TOUCH_CS_PIN);

	///* Send 0x80 to init XP2046 chip */
	//spi_send(SPI2, 0x80);
	//(void)spi_read(SPI2);

	///*Select CS*/
	//gpio_set(ILI9341_TOUCH_CS_PORT, ILI9341_TOUCH_CS_PIN);

	/* Touch interrupt on PC_5 */
	rcc_periph_clock_enable(ILI9341_TOUCH_INT_RCC);
	gpio_mode_setup(ILI9341_TOUCH_INT_PORT, GPIO_MODE_INPUT, GPIO_PUPD_NONE, ILI9341_TOUCH_INT_PIN);

	/* Set the interrupt on PC_5*/
	rcc_periph_clock_enable(RCC_SYSCFG);
	nvic_enable_irq(NVIC_EXTI9_5_IRQ);

	exti_select_source(EXTI5, ILI9341_TOUCH_INT_PORT);
	exti_set_trigger(EXTI5, EXTI_TRIGGER_BOTH);
	exti_enable_request(EXTI5);

	/* TODO remove, test only */
	rcc_periph_clock_enable(RCC_GPIOE);
	gpio_mode_setup(GPIOE, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO2);
	gpio_clear(GPIOE, GPIO2);
}

void exti9_5_isr(void)
{
	E_ili9341_touch_action action = E_TOUCH_ACTION_RELEASE;

	exti_reset_request(EXTI5);

	if(gpio_get(ILI9341_TOUCH_INT_PORT, ILI9341_TOUCH_INT_PIN))
	{
		gpio_set(GPIOE, GPIO2);
		action = E_TOUCH_ACTION_PRESS;
	}
	else
	{
		gpio_clear(GPIOE, GPIO2);
		action = E_TOUCH_ACTION_RELEASE;
	}

	if(registred_user_touch_irq_handler != NULL)
	{
		registred_user_touch_irq_handler(action);
	}
}

static void _init_gpio(void)
{
#if (ILI9341_COMMUNICATION_TYPE == ILI9341_COM_SPI)
	/* Init blacklight pin */
	rcc_periph_clock_enable(ILI9341_BACKLIGHT_RCC);
	gpio_mode_setup(ILI9341_BACKLIGHT_PORT, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, ILI9341_BACKLIGHT_PIN);

	/* Init reset pin */
	rcc_periph_clock_enable(ILI9341_RST_RCC);
	gpio_mode_setup(ILI9341_RST_PORT, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, ILI9341_RST_PIN);

	/* init DC gpio (data/command mode pin)*/
	rcc_periph_clock_enable(ILI9341_DC_RCC);
	gpio_mode_setup(ILI9341_DC_PORT, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, ILI9341_DC_PIN);

	/* init chip select gpio and set it to high (deselect) */
	/*TODO add define for screen CS*/
	gpio_mode_setup(GPIOB, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO12);
	gpio_set(GPIOB, GPIO12);

#elif (ILI9341_COMMUNICATION_TYPE == ILI9341_COM_PARALLELE)

	/* Backlight -> set to 1 */
	rcc_periph_clock_enable(ILI9341_BACKLIGHT_RCC);
	gpio_mode_setup(ILI9341_BACKLIGHT_PORT, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, ILI9341_BACKLIGHT_PIN);
	gpio_set(ILI9341_BACKLIGHT_PORT, ILI9341_BACKLIGHT_PIN);

	/* Display RS (data/command) mode */
	rcc_periph_clock_enable(ILI9341_DC_RCC);
	gpio_mode_setup(ILI9341_DC_PORT, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, ILI9341_DC_PIN);
	gpio_set_output_options(ILI9341_DC_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_100MHZ, ILI9341_DC_PIN);
	gpio_clear(ILI9341_DC_PORT, ILI9341_DC_PIN);

	/* WR (screen write for parallele mode) */
	rcc_periph_clock_enable(ILI9341_SCREEN_WRITE_RCC);
	gpio_mode_setup(ILI9341_SCREEN_WRITE_PORT, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, ILI9341_SCREEN_WRITE_PIN);
	gpio_set_output_options(ILI9341_SCREEN_WRITE_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_100MHZ, ILI9341_SCREEN_WRITE_PIN);
	gpio_set(ILI9341_SCREEN_WRITE_PORT, ILI9341_SCREEN_WRITE_PIN);

	/* RD (read screen for parallele mode) */
	rcc_periph_clock_enable(ILI9341_SCREEN_READ_RCC);
	gpio_mode_setup(ILI9341_SCREEN_READ_PORT, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, ILI9341_SCREEN_READ_PIN);
	gpio_set_output_options(ILI9341_SCREEN_READ_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_100MHZ, ILI9341_SCREEN_READ_PIN);
	gpio_set(ILI9341_SCREEN_READ_PORT, ILI9341_SCREEN_READ_PIN);

	/* Data line */
	/* DB00 */
	rcc_periph_clock_enable(ILI9341_DB00_RCC);
	gpio_mode_setup(ILI9341_DB00_PORT, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, ILI9341_DB00_PIN);
	gpio_set_output_options(ILI9341_DB00_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_100MHZ, ILI9341_DB00_PIN);
	gpio_clear(ILI9341_DB00_PORT, ILI9341_DB00_PIN);

	/* DB01 */
	rcc_periph_clock_enable(ILI9341_DB01_RCC);
	gpio_mode_setup(ILI9341_DB01_PORT, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, ILI9341_DB01_PIN);
	gpio_set_output_options(ILI9341_DB01_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_100MHZ, ILI9341_DB01_PIN);
	gpio_clear(ILI9341_DB01_PORT, ILI9341_DB01_PIN);

	/* DB02 */
	rcc_periph_clock_enable(ILI9341_DB02_RCC);
	gpio_mode_setup(ILI9341_DB02_PORT, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, ILI9341_DB02_PIN);
	gpio_set_output_options(ILI9341_DB02_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_100MHZ, ILI9341_DB02_PIN);
	gpio_clear(ILI9341_DB02_PORT, ILI9341_DB02_PIN);

	/* DB03 */
	rcc_periph_clock_enable(ILI9341_DB03_RCC);
	gpio_mode_setup(ILI9341_DB03_PORT, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, ILI9341_DB03_PIN);
	gpio_set_output_options(ILI9341_DB03_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_100MHZ, ILI9341_DB03_PIN);
	gpio_clear(ILI9341_DB03_PORT, ILI9341_DB03_PIN);

	/* DB04 */
	rcc_periph_clock_enable(ILI9341_DB04_RCC);
	gpio_mode_setup(ILI9341_DB04_PORT, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, ILI9341_DB04_PIN);
	gpio_set_output_options(ILI9341_DB04_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_100MHZ, ILI9341_DB04_PIN);
	gpio_clear(ILI9341_DB04_PORT, ILI9341_DB04_PIN);

	/* DB05 */
	rcc_periph_clock_enable(ILI9341_DB05_RCC);
	gpio_mode_setup(ILI9341_DB05_PORT, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, ILI9341_DB05_PIN);
	gpio_set_output_options(ILI9341_DB05_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_100MHZ, ILI9341_DB05_PIN);
	gpio_clear(ILI9341_DB05_PORT, ILI9341_DB05_PIN);

	/* DB06 */
	rcc_periph_clock_enable(ILI9341_DB06_RCC);
	gpio_mode_setup(ILI9341_DB06_PORT, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, ILI9341_DB06_PIN);
	gpio_set_output_options(ILI9341_DB06_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_100MHZ, ILI9341_DB06_PIN);
	gpio_clear(ILI9341_DB06_PORT, ILI9341_DB06_PIN);

	/* DB07 */
	rcc_periph_clock_enable(ILI9341_DB07_RCC);
	gpio_mode_setup(ILI9341_DB07_PORT, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, ILI9341_DB07_PIN);
	gpio_set_output_options(ILI9341_DB07_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_100MHZ, ILI9341_DB07_PIN);
	gpio_clear(ILI9341_DB07_PORT, ILI9341_DB07_PIN);

	/* DB08 */
	rcc_periph_clock_enable(ILI9341_DB08_RCC);
	gpio_mode_setup(ILI9341_DB08_PORT, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, ILI9341_DB08_PIN);
	gpio_set_output_options(ILI9341_DB08_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_100MHZ, ILI9341_DB08_PIN);
	gpio_clear(ILI9341_DB08_PORT, ILI9341_DB08_PIN);

	/* DB09 */
	rcc_periph_clock_enable(ILI9341_DB09_RCC);
	gpio_mode_setup(ILI9341_DB09_PORT, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, ILI9341_DB09_PIN);
	gpio_set_output_options(ILI9341_DB09_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_100MHZ, ILI9341_DB09_PIN);
	gpio_clear(ILI9341_DB09_PORT, ILI9341_DB09_PIN);

	/* DB10 */
	rcc_periph_clock_enable(ILI9341_DB10_RCC);
	gpio_mode_setup(ILI9341_DB10_PORT, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, ILI9341_DB10_PIN);
	gpio_set_output_options(ILI9341_DB10_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_100MHZ, ILI9341_DB10_PIN);
	gpio_clear(ILI9341_DB10_PORT, ILI9341_DB10_PIN);

	/* DB11 */
	rcc_periph_clock_enable(ILI9341_DB11_RCC);
	gpio_mode_setup(ILI9341_DB11_PORT, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, ILI9341_DB11_PIN);
	gpio_set_output_options(ILI9341_DB11_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_100MHZ, ILI9341_DB11_PIN);
	gpio_clear(ILI9341_DB11_PORT, ILI9341_DB11_PIN);

	/* DB12 */
	rcc_periph_clock_enable(ILI9341_DB12_RCC);
	gpio_mode_setup(ILI9341_DB12_PORT, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, ILI9341_DB12_PIN);
	gpio_set_output_options(ILI9341_DB12_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_100MHZ, ILI9341_DB12_PIN);
	gpio_clear(ILI9341_DB12_PORT, ILI9341_DB12_PIN);

	/* DB13 */
	rcc_periph_clock_enable(ILI9341_DB13_RCC);
	gpio_mode_setup(ILI9341_DB13_PORT, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, ILI9341_DB13_PIN);
	gpio_set_output_options(ILI9341_DB13_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_100MHZ, ILI9341_DB13_PIN);
	gpio_clear(ILI9341_DB13_PORT, ILI9341_DB13_PIN);

	/* DB14 */
	rcc_periph_clock_enable(ILI9341_DB14_RCC);
	gpio_mode_setup(ILI9341_DB14_PORT, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, ILI9341_DB14_PIN);
	gpio_set_output_options(ILI9341_DB14_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_100MHZ, ILI9341_DB14_PIN);
	gpio_clear(ILI9341_DB14_PORT, ILI9341_DB14_PIN);

	/* DB15 */
	rcc_periph_clock_enable(ILI9341_DB15_RCC);
	gpio_mode_setup(ILI9341_DB15_PORT, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, ILI9341_DB15_PIN);
	gpio_set_output_options(ILI9341_DB15_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_100MHZ, ILI9341_DB15_PIN);
	gpio_clear(ILI9341_DB15_PORT, ILI9341_DB15_PIN);

	/* CS_M (screen display chip select) (PD_7) */
	rcc_periph_clock_enable(ILI9341_SCREEN_CS_RCC);
	gpio_mode_setup(ILI9341_SCREEN_CS_PORT, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, ILI9341_SCREEN_CS_PIN);
	gpio_set_output_options(ILI9341_SCREEN_CS_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_100MHZ, ILI9341_SCREEN_CS_PIN);
#endif
}

/* Public function */
int ili9341_fill_screen(uint16_t color)
{
	int ret = 0;

	if(is_initialized == false)
	{
		printf("ili9341 drivers is NOT initialized\n");
		return -1;
	}

	// Set column
	_send_cmd(0x2A);
	_send_data8(0x0000 >> 8);
	_send_data8(0x0000 & 0x00FF);
	_send_data8((ILI9341_SCREEN_COLUMN - 1) >> 8);
	_send_data8((ILI9341_SCREEN_COLUMN - 1) & 0x00FF);

	// Set row
	_send_cmd(0x2B);
	_send_data8(0x0000 >> 8);
	_send_data8(0x0000 & 0x00FF);
	_send_data8((ILI9341_SCREEN_ROW - 1) >> 8);
	_send_data8((ILI9341_SCREEN_ROW - 1) & 0x00FF);

	// Set 'write to RAM'
	_send_cmd(0x2C);

	// Write 320 * 240 pixels.
	for (uint32_t pixel_index = 0; pixel_index < (320*240); ++pixel_index)
	{
		_send_data16(color);
	}

	return 0;
}

int ili9341_set_pixel_frame(uint16_t x_start, uint16_t x_end, uint16_t y_start, uint16_t y_end)
{
	int ret = 0;

	if(is_initialized == false)
	{
		printf("ili9341 drivers is NOT initialized\n");
		return -1;
	}

	if(x_start > x_end)
	{
		printf("ili9341 x_start: %d must be less or equal x_end: %d\n", x_start, x_end);
		return -2;
	}

	if(y_start > y_end)
	{
		printf("ili9341 y_start: %d must be less or equal y_end: %d\n", y_start, y_end);
		return -3;
	}

	/* Check limit */
	if(x_end > ILI9341_SCREEN_COLUMN)
	{
		printf("ili9341 x_end %d must be less than screen column %d\n", x_end, ILI9341_SCREEN_COLUMN);
		return -4;
	}

	if(y_end > ILI9341_SCREEN_ROW)
	{
		printf("ili9341 y_end %d must be less than screen row %d\n", y_end, ILI9341_SCREEN_ROW);
		return -5;
	}

	// Set column
	_send_cmd(0x2A);
	_send_data8(x_start >> 8);
	_send_data8(x_start & 0x00FF);
	_send_data8(x_end >> 8);
	_send_data8(x_end & 0x00FF);

	// Set row
	_send_cmd(0x2B);
	_send_data8(y_start >> 8);
	_send_data8(y_start & 0x00FF);
	_send_data8(y_end >> 8);
	_send_data8(y_end & 0x00FF);

	return 0;
}

int ili9341_send_pixel_data(uint16_t *data, uint32_t nb_data)
{
	// Set 'write to RAM'
	_send_cmd(0x2C);

	for(uint32_t cpt = 0; cpt < nb_data; cpt++)
	{
		_send_data16(data[cpt]);
	}

	return 0;
}

int ili9341_set_pixel_frame_test(uint16_t x_start, uint16_t x_end, uint16_t y_start, uint16_t y_end, uint16_t *data, uint32_t nb_data)
{
	int ret = 0;

	if(is_initialized == false)
	{
		printf("ili9341 drivers is NOT initialized\n");
		return -1;
	}

	if(x_start > x_end)
	{
		printf("ili9341 x_start: %d must be less or equal x_end: %d\n", x_start, x_end);
		return -2;
	}

	if(y_start > y_end)
	{
		printf("ili9341 y_start: %d must be less or equal y_end: %d\n", y_start, y_end);
		return -3;
	}

	/* Check limit */
	if(x_end > ILI9341_SCREEN_COLUMN)
	{
		printf("ili9341 x_end %d must be less than screen column %d\n", x_end, ILI9341_SCREEN_COLUMN);
		return -4;
	}

	if(y_end > ILI9341_SCREEN_ROW)
	{
		printf("ili9341 y_end %d must be less than screen row %d\n", y_end, ILI9341_SCREEN_ROW);
		return -5;
	}

	// Set column
	_send_cmd(0x2A);
	_send_data8(x_start >> 8);
	_send_data8(x_start & 0x00FF);
	_send_data8(x_end >> 8);
	_send_data8(x_end & 0x00FF);

	// Set row
	_send_cmd(0x2B);
	_send_data8(y_start >> 8);
	_send_data8(y_start & 0x00FF);
	_send_data8(y_end >> 8);
	_send_data8(y_end & 0x00FF);

	// Set 'write to RAM'
	_send_cmd(0x2C);

	for(uint32_t cpt = 0; cpt < nb_data; cpt++)
	{
		_send_data16(data[cpt]);
	}

	return 0;
}


int ili9341_send_pixel_data_continue(uint16_t *data, uint32_t nb_data)
{
	// Set 'write to RAM continue'
	_send_cmd(0x3C);

	for(uint32_t cpt = 0; cpt < nb_data; cpt++)
	{
		_send_data16(data[cpt]);
	}

	return 0;
}

int ili9341_set_pixel(uint16_t column, uint16_t row, uint16_t color)
{
	int ret = 0;

	if(is_initialized == false)
	{
		printf("ili9341 drivers is NOT initialized\n");
		return -1;
	}

	if(row > ILI9341_SCREEN_ROW)
	{
		printf("ili9341 row is too big, actual: %d, max: %d\n", row, ILI9341_SCREEN_ROW);
		return -2;
	}

	if(column > ILI9341_SCREEN_COLUMN)
	{
		printf("ili9341 column is too big, actual: %d, max: %d\n", column, ILI9341_SCREEN_COLUMN);
		return -3;
	}

	//printf("set pixel - column: %d, row: %d, color: 0x%X\n", column, row, color);

	// Set column
	_send_cmd(0x2A);
	_send_data8(column >> 8);
	_send_data8(column & 0x00FF);
	_send_data8((ILI9341_SCREEN_COLUMN - 1) >> 8);
	_send_data8((ILI9341_SCREEN_COLUMN - 1) & 0x00FF);

	// Set row
	_send_cmd(0x2B);
	_send_data8(row >> 8);
	_send_data8(row & 0x00FF);
	_send_data8((ILI9341_SCREEN_ROW - 1) >> 8);
	_send_data8((ILI9341_SCREEN_ROW - 1) & 0x00FF);

	// Write pixel color
	_send_cmd(0x2C);
	_send_data16(color);

	return 0;
}


int ili9341_print_test(void)
{
	uint16_t buffer[100];

	/* print red cross x=20 y=20 size=10 color=red*/
	for(int i = 0; i < 20; i++)
	{
		ili9341_set_pixel(i, 10, 0xF840);
		ili9341_set_pixel(10, i, 0xF840);
	}

	/* print square x=220 y=300 size=10 color=blue*/
	for(int i = 0; i<10; i++)
	{
		for(int j = 0; j<10; j++)
		{
			ili9341_set_pixel(220+i, 300+j, 0x081F);
		}
	}

	/*print 10x10 square*/
	memset(buffer, 0xF7E0, sizeof(buffer));
	ili9341_set_pixel_frame(125, 134, 150, 159);
	ili9341_send_pixel_data(buffer, sizeof(buffer)/sizeof(uint16_t));

	return 0;
}

int ili9341_rotate_screen(E_ili9341_screen_rotation_pos pos)
{
	return _change_display_rotation_pos(pos);
}

int ili9341_register_touch_irq_handler(touch_irq_handler irq_handler)
{
	if(irq_handler == NULL)
	{
		printf("Touch irq handler provided is NULL\n");
		return -1;
	}

	registred_user_touch_irq_handler = irq_handler;

	return 0;
}

#define XPT2046_ASK_Z1_VALUE 0xB1
#define XPT2046_ASK_Z2_VALUE 0xC1
#define XPT2046_ASK_X_VALUE 0xD1
#define XPT2046_ASK_Y_VALUE 0x91
int ili9341_get_touch_coordinates(uint16_t *valueX, uint16_t *valueY)
{
	uint16_t z1, z2, x, y;
	int z = 0;

	/* Disable irq before asking value */
	exti_disable_request(EXTI5);

	/*Select CS*/
	gpio_clear(ILI9341_TOUCH_CS_PORT, ILI9341_TOUCH_CS_PIN);

	spi_send(SPI2, XPT2046_ASK_Z1_VALUE);
	/*Dummy read for the first conversion*/
	(void)spi_read(SPI2);

	/* Ask Z2 conversion and read Z1 convered value */
	spi_send(SPI2, XPT2046_ASK_Z2_VALUE);
	z1 = spi_read(SPI2) >> 3;

	/* Ask X conversion and read Z2 convered value */
	spi_send(SPI2, XPT2046_ASK_X_VALUE);
	z2 = spi_read(SPI2) >> 3;

	/* Normalize the pressue value */
	z = (4095 - z2) + z1;
	if(z < 0)
	{
		z = -z;
	}

	/* Ask Y conversion and read X convered value */
	spi_send(SPI2, XPT2046_ASK_Y_VALUE);
	x = spi_read(SPI2) >> 3;

	/* Dummy send to read Y convered value */
	spi_send(SPI2, 0);
	y = spi_read(SPI2) >> 3;

	/*Release CS*/
	gpio_set(ILI9341_TOUCH_CS_PORT, ILI9341_TOUCH_CS_PIN);

	/* Adjust the touch X and Y coordinates according to the screen rotation */
	switch(rotation_pos)
	{
		case E_SCREEN_ROTATION_POS_0:
			*valueX = 4095 - x;
			*valueY = y;
			break;
		case E_SCREEN_ROTATION_POS_1:
			*valueX = x;
			*valueY = y;
			break;
		case E_SCREEN_ROTATION_POS_2:
			*valueX = x;
			*valueY = 4095 - y;
			break;
		case E_SCREEN_ROTATION_POS_3:
			*valueX = 4095 - x;
			*valueY = 4095 - y;
			break;
		default:
			printf("Error new display rotation position is invalide (%d)\n", rotation_pos);
			return -1;
			break;
	}

	/* TODO adjust X - Y value to the calibration value */

	//printf("touch value read, Z:%d, Z1:%d, Z2:%d, X:%d, Y:%d - %s\n", z, z1, z2, x, y, z>300 ? "Press" : "Release");

	if(z > 300)
	{
		/*Pressed*/
		return 1;
	}

	/*Release*/
	return 0;
}


int ili9341_init(void)
{
	int ret = 0;

	if(is_initialized == true)
	{
		printf("ili9341 drivers is already initialized\n");
		return -1;
	}

	/* Init everything needed for the communication */
	_init_gpio();

#if (ILI9341_COMMUNICATION_TYPE == ILI9341_COM_SPI)
	_init_spi();
#endif

	_init_touch();

#if (ILI9341_COMMUNICATION_TYPE == ILI9341_COM_SPI)
	/* Reset and configure the display to be used */
	_reset_display();
#endif

	_send_ili9341_init_sequence();

	is_initialized = true;

	return 0;
}
