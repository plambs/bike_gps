#ifndef _LED_H_
#define _LED_H_

#include <stdbool.h>

typedef enum {
	E_LED_D2 = 0,
	E_LED_D3,
} E_led_id;

int led_set_state(E_led_id led_id, bool state); 
int led_toogle(E_led_id led_id); 
int led_init(void);

#endif
