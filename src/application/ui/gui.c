/*
Copyright (C) 2020 LAMBS Pierre-Antoine

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* Lib C include group */
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

/* Libopencm3 include group */
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/usart.h>
#include <libopencm3/cm3/systick.h>
#include <libopencm3/cm3/nvic.h>

/* FreeRTOS include group */
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"

#include "lvgl.h"
#include "lv_conf.h"

#include "ili9341.h"

#include "gui.h"
#include "log.h"
#include "led.h"

#define SCREEN_HOR_SIZE (240)
#define SCREEN_VER_SIZE (320)
#define LVGL_BUFFER_SIZE ((SCREEN_VER_SIZE * SCREEN_HOR_SIZE) / 10)

#define LVGL_TICK_TIMER pdMS_TO_TICKS(5) /*5ms*/
#define LVGL_REFRESH_RATE_MS pdMS_TO_TICKS(33) /*ms 33ms is around 30 action handling per seconds*/

static bool is_initialized = false;
static lv_disp_draw_buf_t disp_buf;
static lv_color_t buf[LVGL_BUFFER_SIZE];
static TimerHandle_t lvgl_timer_handler = NULL;
static lv_disp_drv_t disp_drv;
static lv_indev_drv_t indev_drv;
static SemaphoreHandle_t gui_mutex = NULL;

static void _take_gui_mutex(void)
{
	xSemaphoreTake(gui_mutex, portMAX_DELAY);
}

static void _release_gui_mutex(void)
{
	xSemaphoreGive(gui_mutex);
}

static void _lvgl_tick(TimerHandle_t xTimer)
{
	(void)xTimer; /* avoid unused parameter warning */

	_take_gui_mutex();
	lv_tick_inc(LVGL_TICK_TIMER);
	_release_gui_mutex();
}

#define LABEL_WIDTH 100
#define LABEL_HEIGHT  65

typedef struct
{
	lv_obj_t *obj;
	char* title;
	char* unit;
	uint16_t value;
} T_home_screen_param;

static T_home_screen_param home_screen[] = {
	 [E_PARAM_SPEED]       = {.title = "Speed",     .value = 0, .unit = "kph"},
	 [E_PARAM_HEART_RATE]  = {.title = "HR",        .value = 0, .unit = "bpm"},
	 [E_PARAM_TEMPERATURE] = {.title = "Temp",      .value = 0, .unit = "°C"},
	 [E_PARAM_CADENCE]     = {.title = "Cadence",   .value = 0, .unit = "rpm"},
	 [E_PARAM_ALTITUDE]    = {.title = "Alt.",      .value = 0, .unit = "m"},
	 [E_PARAM_BATTERY]     = {.title = "Bat.",      .value = 0, .unit = "%"},
	 [E_PARAM_POWER_LEFT]  = {.title = "Pwr Left",  .value = 0, .unit = "W"},
	 [E_PARAM_POWER_RIGHT] = {.title = "Pwr Right", .value = 0, .unit = "W"}
};

static int _create_parameter_label(lv_obj_t *parent_obj, T_home_screen_param *param)
{
	common_die_null(parent_obj, -1, "parent_obj is null\n");
	common_die_null(param, -2, "param is null\n");

	param->obj = lv_label_create(parent_obj);
	lv_obj_set_width(param->obj, LABEL_WIDTH);
	lv_obj_set_height(param->obj, LABEL_HEIGHT);

	lv_label_set_text_fmt(param->obj, "%s\n%d %s", param->title, param->value, param->unit);
	lv_obj_set_style_text_align(param->obj, LV_TEXT_ALIGN_CENTER, 0);

	return 0;
}

/* Build the basic demo UI */
static int _build_ui(void)
{
	int ret = 0;

	lv_obj_t * flex_container = lv_obj_create(lv_scr_act());
	lv_obj_set_size(flex_container, SCREEN_HOR_SIZE, SCREEN_VER_SIZE);
	lv_obj_center(flex_container);
	lv_obj_set_flex_flow(flex_container, LV_FLEX_FLOW_ROW_WRAP);

	for(uint8_t i = 0; i < 8; i++) {
		ret = _create_parameter_label(flex_container, &home_screen[i]);
		common_die_negative(ret, -1, "_create_parameter_label fail, return: %d\n", ret);
	}

	return 0;
}

static void _lvgl_task_handler_thread(void *param)
{
	int ret = 0;

	/* Register the first wake up tick count, after that the vTaskDelayUntil will 
	 * take care of the update */
	TickType_t last_wakeup = xTaskGetTickCount();

	(void)param; /* avoid unused parameter warning */

	/* Init display driver */
	ili9341_init(); /*Init in a task because vTaskDelay in the init */

	/* Build the UI with all the label */
	ret = _build_ui();
	write_log_negative(ret, "Build UI fail, return %d\n", ret);

	while (1)
	{
		/* Let the lvgl lib do the work */
		_take_gui_mutex();
		lv_task_handler();
		_release_gui_mutex();

		/* Wait the time nescessarie to match the wanted ~30fps according to the time
		 * the screen painting/task handling did consume */
		vTaskDelayUntil(&last_wakeup, LVGL_REFRESH_RATE_MS);
	}
}

static void _lvgl_write_on_screen(lv_disp_drv_t * disp, const lv_area_t * area, lv_color_t * color_p)
{
	uint32_t nb_data = (((area->x2 + 1)  - area->x1) * ((area->y2+1) - area->y1));
	TickType_t start_time = xTaskGetTickCount();
	ili9341_set_pixel_frame(area->x1, area->x2, area->y1, area->y2);
	ili9341_send_pixel_data((uint16_t*)color_p, nb_data);

	/* Indicate you are ready with the flushing*/
	lv_disp_flush_ready(disp);
}

static void _touchpad_read(struct _lv_indev_drv_t * indev, lv_indev_data_t * data)
{
	int pressed = 0;
	uint16_t X = 0, Y = 0;

	(void)indev; /* avoid unused parameter warning */

	pressed = ili9341_get_touch_coordinates(&X, &Y);

    data->state = pressed ? LV_INDEV_STATE_PR : LV_INDEV_STATE_REL;
    if(data->state == LV_INDEV_STATE_PR)
	{
		X = ((X * 240) / 4095);
		Y = ((Y * 320) / 4095);

		data->point.x = X;
		data->point.y = Y;
	}
}

int gui_update_param(E_parameter_id param_id, int value)
{
	common_die_not_equal(is_initialized, true, -1, "gui is not initialized\n");
	common_die_superior_or_equal(param_id, E_PARAM_MAX, -2, "param_id is invalid (%d)", param_id);

	if(value == home_screen[param_id].value)
	{
		/* Value didn't change, ignore it */
		return 0;
	}

	/* Update parameter value */
	home_screen[param_id].value = value;

	write_log("New value: %s %d %s\n", home_screen[param_id].title, home_screen[param_id].value, home_screen[param_id].unit);

	/* Update widget*/
	_take_gui_mutex();
	lv_label_set_text_fmt(home_screen[param_id].obj, "%s\n%d %s", home_screen[param_id].title, home_screen[param_id].value, home_screen[param_id].unit);
	_release_gui_mutex();

	return 0;
}

int gui_init(void)
{
	int ret = 0;

	gui_mutex = xSemaphoreCreateMutex();
	common_die_null(gui_mutex, -1, "gui_mutex is null\n");

	/* Basic lvgl init */
	lv_init();

	/* Init the display buffer */
	lv_disp_draw_buf_init(&disp_buf, buf, NULL, LVGL_BUFFER_SIZE);

	/* Init the screen driver */
	lv_disp_drv_init(&disp_drv);
	disp_drv.flush_cb = _lvgl_write_on_screen;
	disp_drv.draw_buf = &disp_buf;
	disp_drv.hor_res = SCREEN_HOR_SIZE;
	disp_drv.ver_res = SCREEN_VER_SIZE;
	disp_drv.sw_rotate = false;
	disp_drv.antialiasing = true;
	lv_disp_drv_register(&disp_drv);

	/* Init the input driver (touchpad) */
	lv_indev_drv_init(&indev_drv);
	indev_drv.type = LV_INDEV_TYPE_POINTER;
	indev_drv.read_cb = _touchpad_read;
	lv_indev_drv_register(&indev_drv);

	lvgl_timer_handler = xTimerCreate("lvgl tick", LVGL_TICK_TIMER, pdTRUE, NULL, _lvgl_tick);
	if(lvgl_timer_handler == NULL)
	{
		write_log("Create lvgl tick timer FAILED\n");
		return -1;
	}

	if(xTimerStart(lvgl_timer_handler, 0) != pdPASS)
	{
		write_log("Start lvgl tick timer FAILED\n");
		return -2;
	}

	if(xTaskCreate(_lvgl_task_handler_thread, "lvgl task handler", 512, NULL, 5, NULL) != pdPASS)
	{
		write_log("Start lvgl handler task FAILED\n");
		return -3;
	}

	is_initialized = true;

	return 0;
}
