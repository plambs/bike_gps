/*
Copyright (C) 2020 LAMBS Pierre-Antoine

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <unistd.h>
#include <stdio.h>

#include <libopencm3/stm32/rcc.h>

#include "FreeRTOS.h"
#include "task.h"

#include "gps.h"
#include "gui.h"
#include "debug.h"
#include "led.h"
#include "log.h"
#include "system.h"

static uint32_t _get_time(void)
{
	return (uint32_t)xTaskGetTickCount();
}

int main(void)
{
	/* Push clockspeed to 168Mhz */
	rcc_clock_setup_pll(&rcc_hse_8mhz_3v3[RCC_CLOCK_3V3_168MHZ]);

	/* Init debug to enable printf */
	debug_init();

	/* Init LED */
	led_init();

	/* Init logging system */
	log_init(&_get_time);
	write_log("Bike GPS V%d.%d.%d\n", MAJOR_VERSION, MINOR_VERSION, REVISION_VERSION);

	/* Init and print gui */
	gui_init();

	/* Init data reception from gps module */
	gps_init();

	/* Start freeRTOS scheduler*/
	write_log("Start FreeRTOS scheduler\n");
	vTaskStartScheduler();

	/* Shouln't end here, ever*/
	write_log("scheduler EXITED\n");
	return 0;
}
