#ifndef _LED_H_
#define _LED_H_

#define LED_1_GPIO 17
#define LED_2_GPIO 18
#define LED_ON 0
#define LED_OFF 1

int led_init(void);
int led_set_state(int led, bool state);
int led_toggle(int led);

#endif
